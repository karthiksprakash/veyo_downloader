Intro

This project is a simple C# console program that simulates a car moving around a city. The goal is to have the car pick up a single passenger and drop the passenger off at the destination.


Some features:

* The passenger wants to contribute to a large number of pageviews for a website. So she loads this website every time the car moves. Using the async/await pattern to download this website on each car move.

* Demonstrates
 - use of IOC framework
 - Async/Await pattern
 - Factory pattern
 - Mediator pattern
