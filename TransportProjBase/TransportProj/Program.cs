﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Threading;
using System.Threading.Tasks;
using Autofac;
using TransportProj.Domain;
using TransportProj.Domain.Interfaces;
using TransportProj.Domain.Models;
using TransportProj.Domain.Models.Vehicles;

namespace TransportProj
{
    [ExcludeFromCodeCoverage]
    class Program
    {
        static void Main(string[] args)
        {
            CancellationTokenSource cts = new CancellationTokenSource();
            Console.CancelKeyPress += (s, e) =>
            {
                e.Cancel = true;
                cts.Cancel();
            };

            try
            {
                MainAsync(args, cts.Token).Wait();
            }
            catch (AggregateException ae)
            {
                foreach (Exception e in ae.InnerExceptions)
                {
                    if (e is TaskCanceledException)
                        Console.WriteLine("Task canceled by user. {0}", e.Message);
                    else
                        Console.WriteLine("{0}. {1}",e.GetType().Name, e.Message);
                }
            }
        }

        private static async Task MainAsync(string[] args, CancellationToken cancellationToken)
        {
            IContainer autofacContainer = BootStrapper.ConfigureAndGetAutofacContainer();

            Random rand = new Random();
            int cityLength = 10;
            int cityWidth = 10;

            ICarFactory carFactory = autofacContainer.Resolve<ICarFactory>(); //TODO: Consider creating a lifetime scope if moving the unit of work below elsewhere.
            ICity myCity = new City(cityLength, cityWidth, carFactory);
            ICar car = myCity.AddCarToCity(CarType.Sedan, rand.Next(cityLength - 1), rand.Next(cityWidth - 1));
            IPassenger passenger = myCity.AddPassengerToCity(rand.Next(cityLength - 1), rand.Next(cityWidth - 1), rand.Next(cityLength - 1), rand.Next(cityWidth - 1));

            await ExecuteTransportOperation(car, passenger, cancellationToken);

            Console.WriteLine("\nPress any key to continue...");
            Console.ReadKey();
        }

        //TODO: Encapsulate this and Tick() below into a "CarRide" class so this method can be unit tested
        private static async Task ExecuteTransportOperation(ICar car, IPassenger passenger, CancellationToken cancellationToken)
        {
            Console.WriteLine($"Initial car position is x = {car.XPos}, y = {car.YPos}");
            Console.WriteLine($"Passenger start position is x = {passenger.StartingXPos}, y = {passenger.StartingYPos}");
            Console.WriteLine(
                $"Passenger destination position is x = {passenger.DestinationXPos}, y = {passenger.DestinationYPos}");

            Console.WriteLine("\nCommencing transportation...");
            while (!passenger.IsAtDestination())
            {
                if (cancellationToken.IsCancellationRequested)
                {
                    Console.WriteLine("\nAborting as requested by user.");
                    cancellationToken.ThrowIfCancellationRequested();
                }
                await Tick(car, passenger);
            }
            await passenger.GetOutOfCar();
            Console.WriteLine("Transport completed!");
        }

        /// <summary>
        /// Takes one action (move the car one spot or pick up the passenger).
        /// </summary>
        /// <param name="car">The car to move</param>
        /// <param name="passenger">The passenger to pick up</param>
        private static async Task Tick(ICar car, IPassenger passenger)
        {
            //TODO: Consider making CarNavigator non-static and pass into car so as to make the car more autonomous
            await CarNavigator.ExecuteDiscreteNavigateAction(car, passenger);
        }


    }
}
