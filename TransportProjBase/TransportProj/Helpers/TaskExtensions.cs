﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportProj.Helpers
{
    public static class TaskExtensions
    {
        //TODO: This is not required in .NET v4.6+ as it comes with a similar implementation.
        public static readonly Task CompletedTask = Task.FromResult(true);
    }
}
