﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MediatR;
using TransportProj.Domain.Interfaces;
using TransportProj.Domain.Models;
using TransportProj.Domain.Models.Vehicles;

namespace TransportProj.Domain
{
    public class CarFactory : ICarFactory
    {
        private readonly IMediator _mediator;

        public CarFactory(IMediator mediator)
        {
            _mediator = mediator;
        }

        public ICar Create(CarType carType, int xPos, int yPos, ICity city)
        {
            if (city == null)
                throw new ArgumentNullException((nameof(city)));
            city.AssertCoordinatesAreWithinBounds(xPos, yPos);

            ICar carToReturn = null;

            switch (carType)
            {
                case CarType.Sedan:
                    carToReturn = new Sedan(xPos, yPos, city, null, _mediator);
                    break;
                case CarType.RaceCar:
                    carToReturn = new RaceCar(xPos, yPos, city, null, _mediator);
                    break;
                case CarType.SedanWithRaceCarEngine:
                    carToReturn = new Sedan(new Engine(2), xPos, yPos, city, null, _mediator);
                    break;
            }

            return carToReturn;
        }
    }
}
