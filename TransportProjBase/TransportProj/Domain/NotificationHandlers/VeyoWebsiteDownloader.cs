﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MediatR;
using TransportProj.Domain.NotificationEvents;

namespace TransportProj.Domain.NotificationHandlers
{
    /// <summary>
    /// Handles a CarMovedEvent by downloading the Veyo homepage.
    /// The website url must be specified through app.config appSettings key "VeyoWebsiteUrl"
    /// </summary>
    public class VeyoWebsiteDownloader : IAsyncNotificationHandler<CarMovedEvent>
    {
        private readonly string _veyoWebSiteUrl;
        private readonly HttpMessageHandler _httpMessageHandler;
        public string DownloadedString { get; set; }

        public VeyoWebsiteDownloader(HttpMessageHandler httpMessageHandler = null)
        {
            _httpMessageHandler = httpMessageHandler;
            _veyoWebSiteUrl = ConfigurationManager.AppSettings["VeyoWebsiteUrl"];
        }

        public async Task Handle(CarMovedEvent notification)
        {
            using (HttpClient httpClient = CreateHttpClient())
            {
                //Store the car position for the current thread.
                int currentXPos = notification.Car.XPos;
                int currentYPos = notification.Car.YPos;

                Console.WriteLine($"Car[{currentXPos}, {currentYPos}]: Downloading page - {_veyoWebSiteUrl}...");
                DownloadedString = await httpClient.GetStringAsync(_veyoWebSiteUrl);
                Console.WriteLine($"Car[{currentXPos}, {currentYPos}]: Download complete - {DownloadedString.Length} bytes.");
            }
        }

        /// <summary>
        /// Creates a new instance of the HttpClient with the default handler if no HttpMessageHandler was specified. 
        /// If one was specified (unit testing), it is used to create a HttpClient.
        /// </summary>
        private HttpClient CreateHttpClient()
        {
            return (_httpMessageHandler == null)
                ? new HttpClient()
                : new HttpClient(_httpMessageHandler, false);
        }
    }
}
