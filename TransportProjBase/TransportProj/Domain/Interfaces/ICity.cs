﻿using TransportProj.Domain.Interfaces;
using TransportProj.Domain.Models.Vehicles;

namespace TransportProj.Domain.Models
{
    public interface ICity
    {
        int YMax { get; }
        int XMax { get; }

        ICar AddCarToCity(CarType carType, int xPos, int yPos);
        IPassenger AddPassengerToCity(int startXPos, int startYPos, int destXPos, int destYPos);
        void AssertNewCoordinatesForCar(int newXPos, int newYPos);
        void AssertCoordinatesAreWithinBounds(int xPos, int yPos);
    }
}