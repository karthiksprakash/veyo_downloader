﻿namespace TransportProj.Domain.Interfaces
{
    public interface IEngine
    {
        int MaxDistancePerMove { get; }
    }
}