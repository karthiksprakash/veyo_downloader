﻿using System.Threading.Tasks;
using TransportProj.Domain.Models;

namespace TransportProj.Domain.Interfaces
{
    public interface ICar
    {
        ICity City { get; }
        IPassenger Passenger { get; }
        IEngine Engine { get; }
        int XPos { get; }
        int YPos { get; }
        
        Task MoveTowardsPosition(int destXPos, int destYPos);
        void PickupPassenger(IPassenger passenger);
        void DropOffPassenger();
    }
}