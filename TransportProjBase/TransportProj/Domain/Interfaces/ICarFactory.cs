﻿using TransportProj.Domain.Models;
using TransportProj.Domain.Models.Vehicles;

namespace TransportProj.Domain.Interfaces
{
    public interface ICarFactory
    {
        ICar Create(CarType carType, int xPos, int yPos, ICity city);
    }
}