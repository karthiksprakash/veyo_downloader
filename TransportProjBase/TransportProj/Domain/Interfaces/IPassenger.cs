﻿using System.Threading.Tasks;
using TransportProj.Domain.Models;

namespace TransportProj.Domain.Interfaces
{
    public interface IPassenger
    {
        ICar Car { get; set; }
        ICity City { get; }
        int DestinationXPos { get; }
        int DestinationYPos { get; }
        int StartingXPos { get; }
        int StartingYPos { get; }

        int GetCurrentXPos();
        int GetCurrentYPos();
        Task GetInCar(ICar car);
        Task GetOutOfCar();
        bool IsAtDestination();
    }
}