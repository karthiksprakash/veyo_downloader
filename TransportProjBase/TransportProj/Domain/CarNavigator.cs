﻿using System.Threading.Tasks;
using TransportProj.Domain.Interfaces;
using TransportProj.Domain.Models;

namespace TransportProj.Domain
{
    public class CarNavigator
    {
        /// <summary>
        /// Performs one discrete action towards the goal of transporting the passenger from his/her start to destination positions. 
        /// The actions can be: 
        /// a. Move the car one spot
        /// b. Pick up the passenger
        /// </summary>
        public static async Task ExecuteDiscreteNavigateAction(ICar car, IPassenger passenger)
        {
            if (car.Passenger == null)
            {
                //The goal here is to pick up the passenger
                if (!IsCarInPositionForPickUp(car, passenger))
                    await car.MoveTowardsPosition(passenger.StartingXPos, passenger.StartingYPos);
                else
                    await passenger.GetInCar(car);
            }
            else
            {
                //The goal here is to transport the passenger
                await car.MoveTowardsPosition(passenger.DestinationXPos, passenger.DestinationYPos);
            }
        }

        /// <summary>
        /// Checks if the car is in the passenger's stating position for pickup
        /// </summary>
        public static bool IsCarInPositionForPickUp(ICar car, IPassenger passenger)
        {
            return (car.XPos == passenger.StartingXPos) && (car.YPos == passenger.StartingYPos);
        }
        
    }
}
