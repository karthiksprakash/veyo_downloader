﻿using System;
using System.Threading.Tasks;
using TransportProj.Domain.Interfaces;

namespace TransportProj.Domain.Models
{
    public class Passenger : IPassenger
    {
        public int StartingXPos { get; }
        public int StartingYPos { get; }
        public int DestinationXPos { get; }
        public int DestinationYPos { get; }
        public ICar Car { get; set; }
        public ICity City { get; private set; }

        public Passenger(int startXPos, int startYPos, int destXPos, int destYPos, ICity city)
        {
            StartingXPos = startXPos;
            StartingYPos = startYPos;
            DestinationXPos = destXPos;
            DestinationYPos = destYPos;
            City = city;
        }

        public async Task GetInCar(ICar car)
        {
            Car = car;
            car.PickupPassenger(this);
            Console.WriteLine("Passenger got in car.");
            await Helpers.TaskExtensions.CompletedTask; 
        }

        public async Task GetOutOfCar()
        {
            if (Car == null)
                throw new InvalidOperationException("Passenger not inside a car.");
            Car.DropOffPassenger();
            Car = null;
            Console.WriteLine("Passenger got out of car.");
            await Helpers.TaskExtensions.CompletedTask;
        }

        public int GetCurrentXPos()
        {
            if(Car == null)
            {
                return StartingXPos;
            }
            else
            {
                return Car.XPos;
            }
        }

        public int GetCurrentYPos()
        {
            if (Car == null)
            {
                return StartingYPos;
            }
            else
            {
                return Car.YPos;
            }
        }

        public bool IsAtDestination()
        {
            return GetCurrentXPos() == DestinationXPos && GetCurrentYPos() == DestinationYPos;
        }
    }
}
