﻿
using System;
using TransportProj.Domain.Interfaces;
using TransportProj.Domain.Models.Vehicles;

namespace TransportProj.Domain.Models
{
    public class City : ICity
    {
        public int YMax { get; }
        public int XMax { get; }

        public ICarFactory CarFactory { get;}

        public City(int xMax, int yMax, ICarFactory carFactory)
        {
            XMax = xMax;
            YMax = yMax;

            if (carFactory == null)
                throw new ArgumentNullException(nameof(carFactory));
            CarFactory = carFactory;
        }

        public ICar AddCarToCity(CarType carType, int xPos, int yPos)
        {
            ICar car = CarFactory.Create(carType, xPos, yPos, this);
            return car;
        }

        public IPassenger AddPassengerToCity(int startXPos, int startYPos, int destXPos, int destYPos)
        {
            AssertCoordinatesAreWithinBounds(startXPos, startYPos);
            AssertCoordinatesAreWithinBounds(destXPos, destYPos);

            Passenger passenger = new Passenger(startXPos, startYPos, destXPos, destYPos, this);

            return passenger;
        }

        /// <summary>
        /// Asserts that the given car can move to the new co-ordinates
        /// </summary>
        public void AssertNewCoordinatesForCar(int xPos, int yPos)
        {
            //TODO: Check for other cars, buildings, etc
            //For now, do a simple boundary validation. 
            AssertCoordinatesAreWithinBounds(xPos, yPos);
        }

        public void AssertCoordinatesAreWithinBounds(int xPos, int yPos)
        {
            if (xPos > XMax)
                throw new ArgumentOutOfRangeException($"X co-ordinate {nameof(xPos)}={xPos} not within city limit of {XMax}.", nameof(xPos));
            if (yPos > YMax)
                throw new ArgumentOutOfRangeException($"Y co-ordinate {nameof(yPos)}={yPos} not within city limit of {YMax}.", nameof(yPos));

            //Do the -ve co-ordinate check here. Consider uint in the future if this'll not be a public API as it is not CLS compliant
            if (xPos < 0)
                throw new ArgumentOutOfRangeException($"Negative co-ordinates are not allowed; {nameof(xPos)}={xPos}", nameof(xPos));
            if (yPos < 0)
                throw new ArgumentOutOfRangeException($"Negative co-ordinates are not allowed; {nameof(yPos)}={yPos}", nameof(yPos));
        }

    }
}
