﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportProj.Domain.Models.Vehicles
{
    public enum CarType
    {
        Sedan,
        RaceCar,
        SedanWithRaceCarEngine
    }
}
