﻿using System;
using System.Threading.Tasks;
using MediatR;
using TransportProj.Domain.Interfaces;
using TransportProj.Domain.NotificationEvents;

namespace TransportProj.Domain.Models.Vehicles
{
    public abstract class Car : ICar
    {
        public IEngine Engine { get; }
        public int XPos { get; protected set; }
        public int YPos { get; protected set; }
        public IPassenger Passenger { get; private set; }
        public IMediator _mediator { get; }
        public int DistancePerMove { get; } = 1;
        public ICity City { get; }

        protected Car(IEngine engine, int xPos, int yPos, ICity city, IPassenger passenger, IMediator mediator)
        {
            //TODO: Some validation can be done when initializing a car
            Engine = engine;
            XPos = xPos;
            YPos = yPos;
            City = city;
            Passenger = passenger;
            _mediator = mediator;
        }

        protected virtual void WritePositionToConsole()
        {
            Console.WriteLine($"Car moved to x = {XPos}, y = {YPos}");
        }

        public void PickupPassenger(IPassenger passenger)
        {
            Passenger = passenger;
        }

        public void DropOffPassenger()
        {
            if (Passenger == null)
                throw new InvalidOperationException("No passenger in car.");
            Passenger = null;
        }

        /// <summary>
        /// Moves the car towards the destination one spot for every call. Hence repeated calls may be necessary to move to the final position.
        /// Traverses X-axis before the Y-axis
        /// </summary>
        public virtual async Task MoveTowardsPosition(int destXPos, int destYPos)
        {
            City.AssertNewCoordinatesForCar(destXPos, destYPos);

            if ((XPos == destXPos) && (YPos == destYPos))
                throw new InvalidOperationException($"This car is already at x = {destXPos}, y = {destYPos}");

            //First move along X axis if necessary
            if (XPos != destXPos)
            {
                int distanceToMove = GetDistanceForMove(XPos, destXPos);
                if (XPos < destXPos)
                    MoveRight(distanceToMove);
                else
                    MoveLeft(distanceToMove);
            }
            //Then move along the Y-axis if necessary
            else 
            {
                int distanceToMove = GetDistanceForMove(YPos, destYPos);
                if (YPos < destYPos)
                    MoveUp(distanceToMove);
                else
                    MoveDown(distanceToMove);
            }

            await _mediator.PublishAsync(new CarMovedEvent(this));
        }

        #region Private helpers

        private void MoveUp(int distance)
        {
            if (YPos < City.YMax)
            {
                YPos += distance;
                WritePositionToConsole();
            }
        }

        private void MoveDown(int distance)
        {
            if (YPos > 0)
            {
                YPos -= distance;
                WritePositionToConsole();
            }
        }

        private void MoveRight(int distance)
        {
            if (XPos < City.XMax)
            {
                XPos += distance;
                WritePositionToConsole();
            }
        }

        private void MoveLeft(int distance)
        {
            if (XPos > 0)
            {
                XPos -= distance;
                WritePositionToConsole();
            }
        }

        /// <summary>
        /// Gets the max distance/tick that we can move given the engine speed and current/destination co-ordinates
        /// </summary>
        private int GetDistanceForMove(int orignCordinate, int destCoordinate)
        {
            int distanceToTravel = Math.Abs(destCoordinate - orignCordinate);
            return Math.Min(distanceToTravel, Engine.MaxDistancePerMove);
        }

        #endregion
    }
}
