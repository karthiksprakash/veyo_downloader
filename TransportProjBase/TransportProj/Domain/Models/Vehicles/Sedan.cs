﻿using System;
using MediatR;
using TransportProj.Domain.Interfaces;

namespace TransportProj.Domain.Models.Vehicles
{
    public class Sedan : Car
    {
        public Sedan(int xPos, int yPos, ICity city, IPassenger passenger, IMediator mediator)
            : base(new Engine(1), xPos, yPos, city, passenger, mediator)
        {
        }

        public Sedan(IEngine engine, int xPos, int yPos, ICity city, IPassenger passenger, IMediator mediator) : base(engine, xPos, yPos, city, passenger, mediator)
        {
        }
        
        protected override void WritePositionToConsole()
        {
            Console.WriteLine($"Sedan moved to x = {XPos}, y = {YPos}");
        }
    }
}
