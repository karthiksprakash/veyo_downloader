﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MediatR;
using TransportProj.Domain.Interfaces;

namespace TransportProj.Domain.Models.Vehicles
{
    public class RaceCar : Car
    {
        public RaceCar(int xPos, int yPos, ICity city, Passenger passenger, IMediator mediator)
            : base(new Engine(2), xPos, yPos, city, passenger, mediator)
        {
        }

        public RaceCar(IEngine engine, int xPos, int yPos, ICity city, Passenger passenger, IMediator mediator) : base(engine, xPos, yPos, city, passenger, mediator)
        {
        }

        protected override void WritePositionToConsole()
        {
            Console.WriteLine($"RaceCar moved to x = {XPos}, y = {YPos}");
        }
    }
}
