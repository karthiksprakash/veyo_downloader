﻿using System;
using TransportProj.Domain.Interfaces;

namespace TransportProj.Domain.Models.Vehicles
{
    public class Engine : IEngine
    {
        public int MaxDistancePerMove { get; }

        public Engine(int maxDistancePerMove)
        {
            MaxDistancePerMove = maxDistancePerMove;
        }
    }
}
