﻿
using TransportProj.Domain.Models.Vehicles;

namespace TransportProj.Domain.Models
{
    public class CityPosition
    {
        public Car Car { get; set; }
        public Passenger Passenger { get; set; }
    }
}
