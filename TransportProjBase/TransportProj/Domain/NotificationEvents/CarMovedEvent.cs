﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MediatR;
using TransportProj.Domain.Interfaces;

namespace TransportProj.Domain.NotificationEvents
{
    public class CarMovedEvent : IAsyncNotification
    {
        public ICar Car { get; set; }

        public CarMovedEvent(ICar car)
        {
            if (car == null)
                throw new ArgumentNullException(nameof(car));
            Car = car;
        }
    }
}
