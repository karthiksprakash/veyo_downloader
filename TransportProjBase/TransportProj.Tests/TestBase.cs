﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MediatR;
using NSubstitute;
using NUnit.Framework;
using TransportProj.Domain.Interfaces;
using TransportProj.Domain.Models;

namespace TransportProj.Tests
{
    [TestFixture, ExcludeFromCodeCoverage]
    abstract class TestBase
    {
        protected IPassenger MockIPassenger;
        protected ICity MockICity;
        protected ICar MockICar;
        protected IMediator MockIMediator;
        
        [SetUp]
        public virtual void Setup()
        {
            MockIPassenger = Substitute.For<IPassenger>();
            MockICar = Substitute.For<ICar>();
            MockICity = Substitute.For<ICity>();
            MockIMediator = Substitute.For<IMediator>();
        }
    }
}
