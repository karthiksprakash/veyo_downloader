﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace TransportProj.Tests.Fakes
{
    [ExcludeFromCodeCoverage]
    class FakeHttpMessageHandler : DelegatingHandler
    {
        private readonly Dictionary<Uri, HttpResponseMessage> _fakeResponses = new Dictionary<Uri, HttpResponseMessage>();

        public void AddFakeResponse(Uri uri, HttpResponseMessage responseMessage)
        {
            _fakeResponses.Add(uri, responseMessage);
        }

        public void AddFakeResponse(string uri, HttpResponseMessage responseMessage)
        {
            _fakeResponses.Add(new Uri(uri), responseMessage);
        }

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, System.Threading.CancellationToken cancellationToken)
        {
            TaskCompletionSource<HttpResponseMessage> response = new TaskCompletionSource<HttpResponseMessage>();
            if (_fakeResponses.ContainsKey(request.RequestUri))
            {
                HttpResponseMessage responseMsg = _fakeResponses[request.RequestUri];
                responseMsg.RequestMessage = request;
                response.SetResult(responseMsg);
                return response.Task;
            }

            response.SetResult(new HttpResponseMessage(HttpStatusCode.NotFound) {RequestMessage = request});
            return response.Task;
        }
    }
}
