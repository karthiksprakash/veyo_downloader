﻿using System;
using System.Diagnostics.CodeAnalysis;
using NSubstitute;
using NUnit.Framework;
using TransportProj.Domain.Interfaces;
using TransportProj.Domain.Models;

namespace TransportProj.Tests
{
    class PassengerTests : TestBase
    {
        [Test]
        public void GetInCar_Succeeds()
        {
            IPassenger passenger = new Passenger(0, 0, 0, 0, MockICity);
            passenger.GetInCar(MockICar);
            MockICar.Received(1).PickupPassenger(passenger);
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException), ExpectedMessage = "Passenger not inside a car.")]
        public async void GetOutOfCar_ThrowsWhenNotInACar()
        {
            IPassenger passenger = new Passenger(0, 0, 0, 0, MockICity);
            await passenger.GetOutOfCar();
        }
    }
}
