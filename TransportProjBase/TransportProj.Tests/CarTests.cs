﻿using System;
using NSubstitute;
using NUnit.Framework;
using TransportProj.Domain.Interfaces;
using TransportProj.Domain.Models.Vehicles;
using TransportProj.Domain.NotificationEvents;

namespace TransportProj.Tests
{
    internal class CarTests : TestBase
    {
        private ICar _testSedan;

        [SetUp]
        public override void Setup()
        {
            base.Setup();
            _testSedan = new Sedan(0, 0, MockICity, null, MockIMediator);
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException), ExpectedMessage = "No passenger in car.")]
        public void DropOffPassenger_ThrowsWhenNotInCar()
        {
            //Test sedan is initialized above with no passenger.
            _testSedan.DropOffPassenger();
        }

        [Test]
        public void MoveTowardsPosition_PublishesCarMovedEvent()
        {
            //Test sedan starts at [0, 0]
            _testSedan.MoveTowardsPosition(1, 1);
            MockIMediator.Received(1).PublishAsync(Arg.Any<CarMovedEvent>());
        }
    }
}
