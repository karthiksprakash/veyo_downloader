﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using NSubstitute;
using NUnit.Framework;
using TransportProj.Domain.NotificationEvents;
using TransportProj.Domain.NotificationHandlers;
using TransportProj.Tests.Fakes;

namespace TransportProj.Tests
{
    class VeyoWebsiteDownloaderTests : TestBase
    {
        private FakeHttpMessageHandler _fakeHttpMessageHandler;
        private readonly string _veyoHomePageContent = "A better future for transportation management.";

        [SetUp]
        public override void Setup()
        {
            base.Setup();

            _fakeHttpMessageHandler = new FakeHttpMessageHandler();
            _fakeHttpMessageHandler.AddFakeResponse("https://veyo.com/",
                new HttpResponseMessage(System.Net.HttpStatusCode.OK)
                {
                    Content = new StringContent(_veyoHomePageContent)
                });
        }

        [Test]
        public async void Handle_Success()
        {
            VeyoWebsiteDownloader veyoWebsiteDownloader = new VeyoWebsiteDownloader(_fakeHttpMessageHandler);
            MockICar.XPos.Returns(5);
            MockICar.YPos.Returns(5);
            CarMovedEvent carMovedEvent = new CarMovedEvent(MockICar);
            await veyoWebsiteDownloader.Handle(carMovedEvent);
            Assert.IsTrue(veyoWebsiteDownloader.DownloadedString.Equals(_veyoHomePageContent,
                StringComparison.OrdinalIgnoreCase));
        }
    }
}
