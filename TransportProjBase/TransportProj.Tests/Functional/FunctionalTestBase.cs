﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;
using TransportProj.Domain;
using TransportProj.Domain.Interfaces;
using TransportProj.Domain.Models;
using TransportProj.Domain.Models.Vehicles;

namespace TransportProj.Tests.Functional
{
    [TestFixture, ExcludeFromCodeCoverage]
    abstract class FunctionalTestBase : TestBase
    {
        protected ICarFactory CarFactory;
        protected ICity City;
        protected Dictionary<string, ICar> TestCars;
        protected Dictionary<string, IPassenger> TestPassengers;
        

        [SetUp]
        public override void Setup()
        {
            base.Setup();

            CarFactory = new CarFactory(MockIMediator);
            City = new City(10, 10, CarFactory);

            TestCars = new Dictionary<string, ICar>()
            {
                {"Sedan0", new Sedan(0, 0, City, null, MockIMediator)},
                {"Sedan1", new Sedan(1, 1, City, null, MockIMediator)},
                {"Sedan8", new Sedan(8, 8, City, null, MockIMediator)},
                {"RaceCar0_Speed2", new RaceCar(0, 0, City, null, MockIMediator) }
            };

            TestPassengers = new Dictionary<string, IPassenger>()
            {
                {"Passenger0", new Passenger(0, 0, 2, 2, City)},
                {"Passenger3", new Passenger(3, 3, 3, 6, City)},
                {"Passenger3x", new Passenger(3, 3, 3, 3, City)}
            };
        }
    }
}
