﻿using System;
using NUnit.Framework;
using TransportProj.Domain.Interfaces;

namespace TransportProj.Tests.Functional
{
    internal class CarTests : FunctionalTestBase
    {
        //[TestCase("Sedan0", destXPos, destYPos, ExpectedResult = MoveCountPerCall)]
        [TestCase("Sedan0", 1, 1, ExpectedResult = 1)]
        [TestCase("Sedan1", 1, 1, ExpectedResult = 0)]
        [TestCase("Sedan1", 1, 4, ExpectedResult = 1)]
        [TestCase("Sedan8", 6, 6, ExpectedResult = 1)]
        public int MoveCarTowardsPosition_CheckMoveCount(string carName, int destXPos, int destYPos)
        {
            int moveCount = 0;
            ICar car = TestCars[carName];

            int initialXPos = car.XPos;
            int initialYPos = car.YPos;
            car.MoveTowardsPosition(destXPos, destYPos);
            moveCount += Math.Abs(car.XPos - initialXPos);
            moveCount += Math.Abs(car.YPos - initialYPos);
            return moveCount;
        }

        //[TestCase("Sedan0", destXPos, destYPos, ExpectedResult = TotalMoveCount)]
        [TestCase("Sedan0", 1, 1, ExpectedResult = 2)]
        [TestCase("Sedan1", 1, 1, ExpectedResult = 0)]
        [TestCase("Sedan1", 1, 4, ExpectedResult = 3)]
        [TestCase("Sedan8", 6, 6, ExpectedResult = 4)]
        public int MoveCarTowardsPosition_CheckTotalMoveCount(string carName, int destXPos, int destYPos)
        {
            int moveCount = 0;
            ICar car = TestCars[carName];
            while ((car.XPos != destXPos) || (car.YPos != destYPos))
            {
                car.MoveTowardsPosition(destXPos, destYPos);
                moveCount++;
            }
            return moveCount;
        }

        //The tests below for a race car have been separated out for better clarity
        //[TestCase("Sedan0", destXPos, destYPos, ExpectedResult = MoveCountPerCall)]
        [TestCase("RaceCar0_Speed2", 5, 0, ExpectedResult = 2)]
        [TestCase("RaceCar0_Speed2", 0, 1, ExpectedResult = 1)]
        public int MoveRaceCarTowardsPosition_CheckMoveCount(string carName, int destXPos, int destYPos)
        {
            return MoveCarTowardsPosition_CheckMoveCount(carName, destXPos, destYPos);
        }

        //[TestCase("Sedan0", destXPos, destYPos, ExpectedResult = TotalMoveCount)]
        [TestCase("RaceCar0_Speed2", 5, 0, ExpectedResult = 3)]
        [TestCase("RaceCar0_Speed2", 5, 9, ExpectedResult = 8)]
        public int MoveRaceCarTowardsPosition_CheckTotalMoveCount(string carName, int destXPos, int destYPos)
        {
            return MoveCarTowardsPosition_CheckTotalMoveCount(carName, destXPos, destYPos);
        }

        public void DropOffPassenger_ThrowsWhenNotInCar()
        {
            
        }
    }
}
