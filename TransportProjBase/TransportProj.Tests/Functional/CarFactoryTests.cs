﻿using System;
using TransportProj.Domain;
using TransportProj.Domain.Interfaces;
using NUnit.Framework;
using TransportProj.Domain.Models.Vehicles;

namespace TransportProj.Tests.Functional
{
    class CarFactoryTests : FunctionalTestBase
    {
        [Test]
        public void CreateSedan_Succeeds()
        {
            CarFactory carFactory = new CarFactory(MockIMediator);
            ICar car = carFactory.Create(CarType.Sedan, 1, 2, City);
            Assert.IsInstanceOf<Sedan>(car);
            Assert.IsTrue(car.City == City, "Car was not assigned to a city");
            Assert.IsTrue(car.XPos == 1, "X coordinates do not match.");
            Assert.IsTrue(car.YPos == 2, "Y coordinates do not match.");
            Assert.IsTrue(car.Engine.MaxDistancePerMove == 1, $"Incorrect engine MaxDistancePerMove should be {1}, but was {car.Engine.MaxDistancePerMove} instead.");
        }

        [Test]
        public void CreateRaceCar_Succeeds()
        {
            CarFactory carFactory = new CarFactory(MockIMediator);
            ICar car = carFactory.Create(CarType.RaceCar, 1, 2, City);
            Assert.IsInstanceOf<RaceCar>(car);
            Assert.IsTrue(car.City == City, "Car was not assigned to a city");
            Assert.IsTrue(car.XPos == 1, "X coordinates do not match.");
            Assert.IsTrue(car.YPos == 2, "Y coordinates do not match.");
            Assert.IsTrue(car.Engine.MaxDistancePerMove == 2, $"Incorrect engine MaxDistancePerMove should be {2}, but was {car.Engine.MaxDistancePerMove} instead.");
        }

        [Test]
        public void CreateSedanWithRaceCar_Succeeds()
        {
            CarFactory carFactory = new CarFactory(MockIMediator);
            ICar car = carFactory.Create(CarType.SedanWithRaceCarEngine, 1, 2, City);
            Assert.IsInstanceOf<Sedan>(car);
            Assert.IsTrue(car.City == City, "Car was not assigned to a city");
            Assert.IsTrue(car.XPos == 1, "X coordinates do not match.");
            Assert.IsTrue(car.YPos == 2, "Y coordinates do not match.");
            Assert.IsTrue(car.Engine.MaxDistancePerMove == 2, $"Incorrect engine MaxDistancePerMove should be {2}, but was {car.Engine.MaxDistancePerMove} instead.");
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Create_ThrowsWhenCityIsNull()
        {
            CarFactory carFactory = new CarFactory(MockIMediator);
            ICar car = carFactory.Create(CarType.SedanWithRaceCarEngine, 1, 2, null);
        }
    }
}
