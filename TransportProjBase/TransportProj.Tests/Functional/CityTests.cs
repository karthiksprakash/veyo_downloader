﻿using System;
using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;
using TransportProj.Domain.Interfaces;
using TransportProj.Domain.Models;
using TransportProj.Domain.Models.Vehicles;

namespace TransportProj.Tests.Functional
{
    class CityTests : FunctionalTestBase
    {
        [TestCase(12, 5, ExpectedException = typeof(ArgumentOutOfRangeException), ExpectedMessage = "X co-ordinate xPos=12 not within city limit of 10.", MatchType = MessageMatch.Contains)]
        [TestCase(2, 15, ExpectedException = typeof(ArgumentOutOfRangeException), ExpectedMessage = "Y co-ordinate yPos=15 not within city limit of 10.", MatchType = MessageMatch.Contains)]
        [TestCase(-1, 0, ExpectedException = typeof(ArgumentOutOfRangeException), ExpectedMessage = "Negative co-ordinates are not allowed; xPos=-1", MatchType = MessageMatch.Contains)]
        [TestCase(0, -1, ExpectedException = typeof(ArgumentOutOfRangeException), ExpectedMessage = "Negative co-ordinates are not allowed; yPos=-1", MatchType = MessageMatch.Contains)]
        public void AssertNewCoordinatesForCar(int newXPos, int newYPos)
        {
            ICity city = new City(10, 10, CarFactory);
            city.AssertNewCoordinatesForCar(newXPos, newYPos);
        }

        [TestCase(5, 5)]
        [TestCase(5, 15, ExpectedException = typeof(ArgumentOutOfRangeException))]
        public void AddSedanToCity_Succeeds(int xPos, int yPos)
        {
            ICar sedan = City.AddCarToCity(CarType.Sedan, xPos, yPos);
            Assert.IsNotNull(sedan, "No car was returned.");
            Assert.IsTrue(sedan.City == City, "City was not assigned correctly to the created car");
        }
    }
}
