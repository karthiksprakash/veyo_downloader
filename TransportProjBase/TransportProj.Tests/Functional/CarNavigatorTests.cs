﻿using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using NUnit.Framework;
using TransportProj.Domain;
using TransportProj.Domain.Interfaces;

namespace TransportProj.Tests.Functional
{
    class CarNavigatorTests : FunctionalTestBase
    {
        [TestCase("Sedan0", "Passenger0", ExpectedResult = true)]
        [TestCase("Sedan1", "Passenger3", ExpectedResult = false)]
        [TestCase("Sedan1", "Passenger3x", ExpectedResult = false)]
        public bool IsCarInPositionForPickUp(string carName, string passengerName)
        {
            return CarNavigator.IsCarInPositionForPickUp(TestCars[carName], TestPassengers[passengerName]);
        }
        

        //[TestCase("CarToUse", "PassengerToPickUp", ExpectedResult = TotalTicksTaken)]
        [TestCase("Sedan0", "Passenger0", ExpectedResult = 5)]
        [TestCase("Sedan1", "Passenger3", ExpectedResult = 8)]
        [TestCase("RaceCar0_Speed2", "Passenger3", ExpectedResult = 7)]
        public async Task<int> ExecuteDiscreteNavigateAction(string carName, string passengerName)
        {
            int tickCount = 0;
            ICar car = TestCars[carName];
            IPassenger passenger = TestPassengers[passengerName];
            while (!passenger.IsAtDestination())
            {
                await CarNavigator.ExecuteDiscreteNavigateAction(car, passenger);
                tickCount++;
            }

            Assert.IsNotNull(passenger.Car, "Passenger was not picked up.");
            Assert.IsTrue(passenger.IsAtDestination(),
                "Passenger not at destination [{0},{1}], but instead is at [{2},{3}]", passenger.DestinationXPos,
                passenger.DestinationYPos, passenger.GetCurrentXPos(), passenger.GetCurrentYPos());
            
            return tickCount;
        }

    }
}
